﻿using MLFramework;
using MLFramework.Events;
using UnityEngine;

public class InputManager : SingletonMonoBehaviour<InputManager>
{
    private Vector2 m_LeftStick;
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Init()
    {
        Instance.EnableDontDestroy();
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            EventsManager.TriggerEvent(EventID.APressed);

        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
            EventsManager.TriggerEvent(EventID.BPressed);

        

        m_LeftStick.x = UnityEngine.Input.GetAxis("Horizontal");
        m_LeftStick.y = UnityEngine.Input.GetAxis("Vertical");
        EventsManager.TriggerEvent(EventID.LeftStickMoved, this.transform, m_LeftStick);
    }
}