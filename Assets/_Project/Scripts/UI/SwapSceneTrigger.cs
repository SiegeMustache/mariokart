﻿using UnityEngine;

public class SwapSceneTrigger : MonoBehaviour
{
    [SerializeField] private Animator m_TargetAnimator;
    [SerializeField] private bool m_EnableMainMenu;
    [SerializeField] private bool m_EnableModeMenu;
    [SerializeField] private bool m_EnableInGame;

    public void CallTrigger()
    {
        m_TargetAnimator.SetBool("MainMenu", m_EnableMainMenu);
        m_TargetAnimator.SetBool("ModeMenu", m_EnableModeMenu);
        m_TargetAnimator.SetBool("InGame", m_EnableInGame);
        m_TargetAnimator.SetTrigger("SwapScene");
    }
}
