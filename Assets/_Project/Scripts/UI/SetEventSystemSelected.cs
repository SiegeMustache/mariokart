﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SetEventSystemSelectedOnEnable : MonoBehaviour
{
    private void OnEnable()
    {
        var eventSystem = GameObject.FindWithTag("EventSystem").GetComponent<EventSystem>();
        eventSystem.SetSelectedGameObject(gameObject);
    }
}
