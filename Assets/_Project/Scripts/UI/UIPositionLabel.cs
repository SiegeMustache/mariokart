﻿using MarioKart.Track;
using MLFramework.Core;
using TMPro;
using UnityEngine;

namespace MLFramework.Events.UI
{
public class UIPositionLabel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_PositionText;

    private PlayerController m_TargetPlayer;

    private void OnEnable()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_TargetPlayer = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_PositionText.text = (12 - RaceManager.GetLeaderboard().IndexOf(m_TargetPlayer)).ToString();
    }
}
}