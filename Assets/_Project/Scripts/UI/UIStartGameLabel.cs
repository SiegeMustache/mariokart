﻿using System;
using TMPro;
using UnityEngine;

namespace MLFramework.Events.UI
{
public class UIStartGameLabel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_StartGameText;
    
    private void OnEnable()
    {
        EventsManager.StartListening(EventID.RaceStartCounterUpdate, OnRaceCountUpdate);
    }

    private void OnDisable()
    {
        EventsManager.StopListening(EventID.RaceStartCounterUpdate, OnRaceCountUpdate);
    }

    private void OnRaceCountUpdate(GenericEventArgs args)
    {
        var value = (int) args.Args[0];
        if (value == 0)
            m_StartGameText.text = "";
        else
            m_StartGameText.text = value.ToString();
    }
}
}