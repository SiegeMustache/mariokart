﻿using MarioKart.Movement;
using MLFramework.Core;
using UnityEngine.SceneManagement;

public static class PlayerManager
{
    private const string GAME_SCENE = "SCN_BabyPark";
    
    public static KartStats SelectedStats { get; private set; }

    private static bool m_Loading = false;
    
    public static void SelectKartAndStartGame(KartStats selectedKart)
    {
        if (!m_Loading)
        {
            m_Loading = true;
            SelectedStats = selectedKart;
            SceneManager.sceneLoaded += OnGameSceneLoaded;
            SceneManager.LoadSceneAsync(GAME_SCENE, LoadSceneMode.Additive);
        }
    }

    private static void OnGameSceneLoaded(Scene scn, LoadSceneMode mode)
    {
        m_Loading = false;
        SceneManager.sceneLoaded -= OnGameSceneLoaded;
        GameManager.ChangeState(GameStates.Gameplay);
        SceneManager.SetActiveScene(scn);
        RaceManager.StartRace();
    }
}