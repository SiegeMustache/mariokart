﻿using MarioKart.Track;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueShellObject : PowerUpObject
{
    public GameObject ExplosionObject;
    [HideInInspector] public PowerUpHandler PowerUpOwner;
    [SerializeField] private float m_TargetingRange;
    [HideInInspector] public PowerUpHandler Target;
    [SerializeField] private float m_MovementSpeed;
    [SerializeField] private float m_DistanceFromGround;
    private Rigidbody m_Rb;
    private TrackMover m_TrackMover;
    private float m_SplineStartOffset;
    private bool m_LockedIn = false;
    private float m_LockInTimer = 2f;

    // Start is called before the first frame update
    void Start()
    {
        m_Rb = GetComponentInChildren<Rigidbody>();
        m_TrackMover = GetComponentInChildren<TrackMover>();
        m_TrackMover.SetupMover(PowerUpOwner.transform.parent.GetComponentInChildren<TrackProgressor>().ActualProgression, true, m_DistanceFromGround, m_MovementSpeed, true, true, true);
        m_LockedIn = false;
        m_LockInTimer = 2f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, Target.transform.position) <= m_TargetingRange || m_LockedIn)
        {
            m_LockedIn = true;

            transform.position = new Vector3(Target.transform.position.x, Target.transform.position.y + 2, Target.transform.position.z);
            m_LockInTimer -= Time.fixedDeltaTime;
            if (m_LockInTimer <= 0)
            {
                GameObject go = Instantiate(ExplosionObject, Target.transform.position, Quaternion.identity);
                Destroy(this.gameObject);
            }
        }
        else
        {
            MoveTowardsTargetSpline(m_TrackMover.FollowPosition);
        }
    }

    private void MoveTowardsTargetKart(PowerUpHandler target)
    {
        var movement = transform.position + ((target.transform.position - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);
        RaycastHit hit;
        if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
        {
            var targetY = hit.point.y + m_DistanceFromGround;
            movement.y = targetY;
        }

        m_Rb.MovePosition(movement);
    }

    private void MoveTowardsTargetSpline(Vector3 target)
    {
        var movement = transform.position + ((target - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);

        m_Rb.MovePosition(movement);
    }
}
