﻿using MarioKart.Track;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BlueShellPowerUp : PowerUp
{
    public GameObject BlueShellObject;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        List<PlayerController> leaderBoard = RaceManager.GetLeaderboard();
        PowerUpHandler leaderHandler = leaderBoard[11].GetComponentInChildren<PowerUpHandler>();

        base.UsePowerUp(go, direction);
        GameObject obj = Instantiate(BlueShellObject, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
        obj.GetComponentInChildren<BlueShellObject>().PowerUpOwner = handler;
        obj.GetComponentInChildren<BlueShellObject>().Target = leaderHandler;
        
    }
}
