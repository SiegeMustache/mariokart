﻿using MarioKart.Track;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedShellObject : PowerUpObject
{
    public List<Effect> RedShellEffects;
    [HideInInspector]
    public PowerUpHandler PowerUpOwner;
    [SerializeField] private float m_TargetingRange;
    [SerializeField] private LayerMask m_TargetLayerMask;
    private PowerUpHandler m_Target;
    [SerializeField] private float m_MovementSpeed;
    [SerializeField] private float m_DistanceFromGround;
    private Rigidbody m_Rb;
    private TrackMover m_TrackMover;
    private float m_SplineStartOffset;

    // Start is called before the first frame update
    void Start()
    {
        m_Rb = GetComponentInChildren<Rigidbody>();
        m_TrackMover = GetComponentInChildren<TrackMover>();
        m_TrackMover.SetupMover(PowerUpOwner.transform.parent.GetComponentInChildren<TrackProgressor>().ActualProgression, true, m_DistanceFromGround, m_MovementSpeed, true, true, true);
        m_Target = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_Target == null && m_Target != PowerUpOwner)
        {
            m_Target = GetTarget();
            MoveTowardsTargetSpline(m_TrackMover.FollowPosition);
        }
        else if(m_Target != null)
        {
            m_TrackMover.enabled = false;
            MoveTowardsTargetKart(m_Target);
        }
    }

    private PowerUpHandler GetTarget()
    {
        Collider[] targetsNearby = Physics.OverlapSphere(transform.position, m_TargetingRange, m_TargetLayerMask);
        List<PowerUpHandler> targetsFiltered = new List<PowerUpHandler>();

        foreach(Collider collider in targetsNearby)
        {
            if (collider.GetComponentInChildren<PowerUpHandler>())
            {
                if (collider.GetComponentInChildren<PowerUpHandler>() != PowerUpOwner)
                {
                    targetsFiltered.Add(collider.GetComponentInChildren<PowerUpHandler>());
                }
            }
        }

        targetsFiltered.Remove(PowerUpOwner);

        if(targetsFiltered.Count > 0)
        {
            PowerUpHandler target = targetsFiltered[0];

            foreach (PowerUpHandler handler in targetsFiltered)
            {
                if(Vector3.Distance(transform.position, handler.transform.position) > Vector3.Distance(transform.position, target.transform.position))
                {
                    target = handler;
                }
            }
            return target;
        }
        return null;
    }

    private void MoveTowardsTargetKart(PowerUpHandler target)
    {
        var movement = transform.position + ((target.transform.position - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);
        RaycastHit hit;
        if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
        {
            var targetY = hit.point.y + m_DistanceFromGround;
            movement.y = targetY;
        }

        m_Rb.MovePosition(movement);
    }

    private void MoveTowardsTargetSpline(Vector3 target)
    {
        var movement = transform.position + ((target - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);

        m_Rb.MovePosition(movement);
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.transform.parent != null && otherCollider.transform.parent.GetComponentInChildren<PowerUpHandler>() && !otherCollider.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            var handler = otherCollider.transform.parent.GetComponentInChildren<PowerUpHandler>();
            if (handler)
            {
                if(handler != PowerUpOwner)
                {
                    if (!handler.IsImmune)
                    {
                        if(handler.MainPowerUp != null)
                        {
                            if (handler.MainPowerUp.RotatorModel.Count > 0)
                            {
                                handler.UpdatePowerUpUsage();
                            }
                            else
                            {
                                foreach (var effect in RedShellEffects)
                                {
                                    effect.ApplyEffect(handler.m_Mover);
                                }
                                handler.DropCoins();
                            }
                        }
                        else
                        {
                            foreach (var effect in RedShellEffects)
                            {
                                effect.ApplyEffect(handler.m_Mover);
                            }
                            handler.DropCoins();
                        }
                    }
                    Destroy(this.transform.gameObject);
                }
            }
        }
    }
}
