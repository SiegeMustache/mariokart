﻿using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BulletBillPowerUp : PowerUp
{
    public GameObject BulletBillObject;
    private GameObject m_ActiveBulletBill;
    private PowerUpHandler m_Owner;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();
        m_Owner = handler;
        m_Owner.m_Mover.GetComponent<KartMover>().Drift(false);
        m_Owner.m_Mover.GetComponent<KartMover>().SetDrift(false);
        if (m_ActiveBulletBill == null)
        {
            m_ActiveBulletBill = Instantiate(BulletBillObject, handler.FrontSpawner.transform.position, Quaternion.identity);
            m_ActiveBulletBill.transform.forward = handler.FrontSpawner.transform.forward;
            m_ActiveBulletBill.GetComponentInChildren<BulletBillObject>().PowerUpOwner = handler;
            handler.SetImmunity(true);
        }
        
    }

    public override void RemovePowerUp()
    {
        if (m_ActiveBulletBill != null)
        {
            m_Owner.SetImmunity(false);
            m_Owner.GetComponent<Rigidbody>().freezeRotation = false;
            m_Owner.GetComponent<Rigidbody>().isKinematic = false;
            m_Owner.GetComponent<Rigidbody>().velocity = Vector3.zero;
            m_Owner.m_Mover.GetComponent<KartMover>().Enabled = true;
            Destroy(m_ActiveBulletBill);
        }
        else if (m_ActiveBulletBill == null)
        {
            Debug.Log("WAT");
        }
    }
}
