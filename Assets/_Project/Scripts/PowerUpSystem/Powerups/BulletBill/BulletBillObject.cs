﻿using MarioKart.Movement;
using MarioKart.Track;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBillObject : MonoBehaviour
{

    [HideInInspector] public PowerUpHandler PowerUpOwner;
    [SerializeField] private float m_MovementSpeed;
    [SerializeField] private float m_DistanceFromGround;
    private Rigidbody m_Rb;
    private TrackMover m_TrackMover;
    private float m_SplineStartOffset;

    // Start is called before the first frame update
    void Start()
    {
        m_Rb = GetComponentInChildren<Rigidbody>();
        m_TrackMover = GetComponentInChildren<TrackMover>();
        m_TrackMover.SetupMover(PowerUpOwner.transform.parent.GetComponentInChildren<TrackProgressor>().ActualProgression, true, m_DistanceFromGround, m_MovementSpeed, true, true, true);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        PowerUpOwner.transform.position = transform.position;
        PowerUpOwner.transform.rotation = transform.rotation;
        PowerUpOwner.m_Mover.transform.position = transform.position;
        PowerUpOwner.m_Mover.transform.rotation = transform.rotation;
        PowerUpOwner.m_Mover.GetComponent<KartMover>().Enabled = false;
        PowerUpOwner.GetComponent<Rigidbody>().freezeRotation = true;
        PowerUpOwner.GetComponent<Rigidbody>().isKinematic = true;
        MoveTowardsTargetSpline(m_TrackMover.FollowPosition);
    }

    private void MoveTowardsTargetSpline(Vector3 target)
    {
        var movement = transform.position + ((target - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);

        m_Rb.MovePosition(movement);
    }
}
