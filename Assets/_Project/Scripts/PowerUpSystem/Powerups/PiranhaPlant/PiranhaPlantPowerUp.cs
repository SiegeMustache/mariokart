﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PiranhaPlantPowerUp : PowerUp
{
    public GameObject PiranhaPlantObject;
    private GameObject m_ActivePiranhaPlant;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();
        if(m_ActivePiranhaPlant == null)
        {
            m_ActivePiranhaPlant = Instantiate(PiranhaPlantObject, handler.FrontSpawner.transform.position, Quaternion.identity);
            m_ActivePiranhaPlant.transform.forward = handler.FrontSpawner.transform.forward;
            m_ActivePiranhaPlant.GetComponentInChildren<PiranhaPlantObject>().PowerUpOwner = handler;
        }
        else if(m_ActivePiranhaPlant != null)
        {
            m_ActivePiranhaPlant.GetComponentInChildren<PiranhaPlantObject>().UsePiranhaPlant();
        }
    }

    public override void RemovePowerUp()
    {
        if(m_ActivePiranhaPlant != null)
        {
            Destroy(m_ActivePiranhaPlant);
        }
        else if(m_ActivePiranhaPlant == null)
        {
            Debug.Log("WAT");
        }
    }
}
