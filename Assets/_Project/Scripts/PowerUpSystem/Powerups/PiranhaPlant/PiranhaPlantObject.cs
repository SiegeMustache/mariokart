﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaPlantObject : MonoBehaviour
{
    [HideInInspector] public PowerUpHandler PowerUpOwner;
    [SerializeField] private Effect m_OffensivePiranhaPlantEffect;
    [SerializeField] private Effect m_BoostPiranhaPlantEffect;
    [SerializeField] private LayerMask m_KartTargetLayer;
    [SerializeField] private LayerMask m_ObjectsTargetLayer;
    private GameObject m_Target;
    [SerializeField] private float m_TargetingRange;
    private float m_Timer;

    private void Start()
    {
        m_Timer = 0;
    }

    private void Update()
    {
        transform.position = PowerUpOwner.FrontSpawner.transform.position;
        transform.forward = PowerUpOwner.FrontSpawner.transform.forward;
        m_Timer += Time.deltaTime;
        if(m_Timer >= 3)
        {
            m_BoostPiranhaPlantEffect.ApplyEffect(PowerUpOwner.m_Mover);
            m_Timer = 0;
        }
    }

    public void UsePiranhaPlant()
    {
        m_Target = PiranhaPlantLunge();
        AttackTarget(m_Target);
    }

    private GameObject PiranhaPlantLunge()
    {
        Collider[] targetsNearby = Physics.OverlapSphere(transform.position, m_TargetingRange, m_KartTargetLayer);
        List<PowerUpHandler> targetsFiltered = new List<PowerUpHandler>();

        foreach (Collider collider in targetsNearby)
        {
            if (collider.GetComponentInChildren<PowerUpHandler>())
            {
                if (collider.GetComponentInChildren<PowerUpHandler>() != PowerUpOwner)
                {
                    targetsFiltered.Add(collider.GetComponentInChildren<PowerUpHandler>());
                }
            }
        }

        targetsFiltered.Remove(PowerUpOwner);
        PowerUpHandler nearestKart = new PowerUpHandler();

        if(targetsFiltered.Count > 0)
        {
            for (int i = 0; i < targetsFiltered.Count; i++)
            {
                if(i == 0)
                {
                    nearestKart = targetsFiltered[i];
                }
                else
                {
                    if(Vector3.Distance(nearestKart.transform.position, transform.position) > Vector3.Distance(targetsFiltered[i].transform.position, transform.position))
                    {
                        nearestKart = targetsFiltered[i];
                    }
                }
            }
        }


        Collider[] objectsNearby = Physics.OverlapSphere(transform.position, m_TargetingRange, m_ObjectsTargetLayer);
        PowerUpObject nearestObject = new PowerUpObject();
        
        if(objectsNearby.Length > 0)
        {
            for(int i = 0; i < objectsNearby.Length; i++)
            {
                if(i == 0)
                {
                    nearestObject = objectsNearby[i].transform.GetComponentInChildren<PowerUpObject>();
                }
                else
                {
                    if(Vector3.Distance(nearestObject.transform.position, transform.position) > Vector3.Distance(objectsNearby[i].transform.position, transform.position))
                    {
                        nearestObject = objectsNearby[i].transform.GetComponentInChildren<PowerUpObject>();
                    }
                }
            }
        }

        if(nearestObject == null && nearestKart != null)
        {
            return m_Target = nearestKart.transform.parent.gameObject;
        }
        else if(nearestObject != null && nearestKart == null)
        {
            return m_Target = nearestObject.transform.gameObject;
        }
        else if(nearestObject != null && nearestKart != null)
        {
            if(Vector3.Distance(nearestKart.transform.position, transform.position) > Vector3.Distance(nearestObject.transform.position, transform.position))
            {
                return nearestObject.transform.gameObject;
            }
            else
            {
                return nearestKart.transform.parent.gameObject;
            }
        }

        return null;
    }

    private void AttackTarget(GameObject target)
    {
        if(target != null)
        {
            if(target.GetComponentInChildren<PowerUpObject>() != null)
            {
                Destroy(target);
            }
            else if(target.GetComponentInChildren<PowerUpHandler>())
            {
                PowerUpHandler targetHandler = target.GetComponentInChildren<PowerUpHandler>();

                if(!targetHandler.IsImmune)
                {
                    if(targetHandler.MainPowerUp != null)
                    {
                        PowerUpOwner.AssignPowerUp(targetHandler.StealCurrentPowerUp());
                    }
                    m_OffensivePiranhaPlantEffect.ApplyEffect(targetHandler.m_Mover);
                    targetHandler.DropCoins();
                }
                m_BoostPiranhaPlantEffect.ApplyEffect(PowerUpOwner.m_Mover);
            }
        }
    }
}
