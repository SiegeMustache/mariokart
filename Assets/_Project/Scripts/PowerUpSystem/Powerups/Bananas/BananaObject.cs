﻿using MarioKart.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaObject : PowerUpObject
{
    public List<Effect> BananaEffects = new List<Effect>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.GetComponentInChildren<PowerUpHandler>() && !other.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            PowerUpHandler handler = other.transform.parent.GetComponentInChildren<PowerUpHandler>();

            if (!handler.IsImmune)
            {
                if(handler.MainPowerUp != null)
                {
                    if(handler.MainPowerUp.RotatorModel.Count > 0)
                    {
                        handler.UpdatePowerUpUsage();
                    }
                    else
                    {
                        foreach (var effect in BananaEffects)
                        {
                            effect.ApplyEffect(handler.m_Mover);
                        }
                        handler.DropCoins();
                    }
                }
                else
                {
                    foreach (var effect in BananaEffects)
                    {
                        effect.ApplyEffect(handler.m_Mover);
                    }
                    handler.DropCoins();
                }
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            if(GetComponent<Rigidbody>())
            {
                GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
}
