﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpRotator : MonoBehaviour
{
    public GameObject model;
    public float rotateSpeed;

    // Update is called once per frame
    void FixedUpdate()
    {
        if(model != null)
        {
            model.transform.Rotate(0, rotateSpeed, 0);
            model.transform.position = transform.position;
        }
    }

    public void AddModel(GameObject view)
    {
        GameObject go = Instantiate(view, transform.position, Quaternion.identity);
        model = go;
    }

    public void RemoveModel()
    {
        if(model != null)
        {
            Destroy(model);
            model = null;
        }
    }
}
