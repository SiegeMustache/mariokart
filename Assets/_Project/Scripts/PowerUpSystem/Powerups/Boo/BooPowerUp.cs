﻿using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BooPowerUp : PowerUp
{
    private PowerUpHandler m_Owner;
    public Effect BooEffect;
    [SerializeField] private LayerMask m_TargetLayerMask;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();
        BooEffect.ApplyEffect(go);
        m_Owner = handler;

        Collider[] targetsNearby = Physics.OverlapSphere(m_Owner.transform.position, 100f, m_TargetLayerMask);
        List<PowerUpHandler> targetsFiltered = new List<PowerUpHandler>();

        if(targetsNearby.Length > 0)
        {
            foreach (Collider collider in targetsNearby)
            {
                if (collider.GetComponentInChildren<PowerUpHandler>())
                {
                    if (collider.GetComponentInChildren<PowerUpHandler>() != m_Owner)
                    {
                        if (collider.GetComponentInChildren<PowerUpHandler>().MainPowerUp != null)
                        {
                            targetsFiltered.Add(collider.GetComponentInChildren<PowerUpHandler>());
                        }
                    }
                }
            }

            targetsFiltered.Remove(m_Owner);

            if(targetsFiltered.Count > 0)
            {
                PowerUpHandler target = targetsFiltered[0];

                for (int i = 0; i< targetsFiltered.Count; i++)
                {
                    if(i > 0)
                    {
                        if(Vector3.Distance(m_Owner.transform.position, targetsFiltered[i].transform.position) < Vector3.Distance(target.transform.position, m_Owner.transform.position))
                        {
                            target = targetsFiltered[i];
                        }
                    }
                }

                m_Owner.AssignPowerUp(target.StealCurrentPowerUp());
            }
        }
    }
}
