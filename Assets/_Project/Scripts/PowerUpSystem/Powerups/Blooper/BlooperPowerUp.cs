﻿using MarioKart.Input;
using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BlooperPowerUp : PowerUp
{
    [SerializeField] private Effect BlooperAIEffect;
    private PowerUpHandler m_Owner;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        List<PlayerController> players = RaceManager.GetLeaderboard();
        m_Owner = go.transform.parent.GetComponentInChildren<PowerUpHandler>();
        int currentPosition;

        for(int i = 0; i < players.Count; i++)
        {
            if(players[i] == m_Owner.transform.parent.GetComponent<PlayerController>())
            {
                currentPosition = i;
                if(currentPosition < players.Count -1)
                {
                    for (int j = currentPosition + 1; j < players.Count; j++)
                    {
                        if (players[j].GetComponentInChildren<InputHandler>())
                        {
                            InkStainController.Instance.ActivateStain();
                        }
                        else
                        {
                            BlooperAIEffect.ApplyEffect(players[j].GetComponentInChildren<KartMover>().gameObject);
                        }
                    }
                    return;
                }
            }
        }
    }
}
