﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionObject : MonoBehaviour
{
    public float StunThreshold;
    public float LifeTime;
    private float m_Timer;
    public Effect StunEffect;
    public Effect DisableEffect;
    private Effect m_CurrentEffect;

    private void OnEnable()
    {
        m_Timer = LifeTime;
        m_CurrentEffect = StunEffect;
    }

    private void Update()
    {
        m_Timer -= Time.deltaTime;
        if(LifeTime - m_Timer > StunThreshold)
        {
            m_CurrentEffect = DisableEffect;
        }
        if(m_Timer <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.GetComponentInChildren<PowerUpHandler>() && !other.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            PowerUpHandler handler = other.transform.parent.GetComponentInChildren<PowerUpHandler>();

            if (!handler.IsImmune)
            {
                if (handler.MainPowerUp != null)
                {
                    handler.RemoveCurrentPowerUp();
                    m_CurrentEffect.ApplyEffect(handler.m_Mover);
                    handler.DropCoins();
                }
                else
                {
                    m_CurrentEffect.ApplyEffect(handler.m_Mover);
                    handler.DropCoins();
                }
            }
        }
    }

}
