﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobombObject : PowerUpObject
{
    public GameObject ExplosionObject;
    public float LifeTime;
    private float m_Timer;
    private bool m_IsArmed = false;

    private void Update()
    {
        if(m_IsArmed)
        {
            if (m_Timer > 0)
            {
                m_Timer -= Time.deltaTime;
            }
            else
            {
                GameObject go = Instantiate(ExplosionObject, transform.position, Quaternion.identity);
                Destroy(this.gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.GetComponentInChildren<PowerUpHandler>() && !other.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            GameObject go = Instantiate(ExplosionObject, transform.position, Quaternion.identity);
            Destroy(this.gameObject);  
        }
        else
        {
            if (GetComponent<Rigidbody>())
            {
                GetComponent<Rigidbody>().isKinematic = true;
                m_IsArmed = true;
                m_Timer = LifeTime;
            }
        }
    }
}
