﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BobombPowerUp : PowerUp
{
    public GameObject BobombObject;
    public float HeightForce;
    public float LengthForce;
    public float GravityForce;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        switch (direction)
        {
            case PowerUpHandler.UseDirection.Behind:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(BobombObject, handler.BackSpawner.transform.position, Quaternion.LookRotation(handler.BackSpawner.transform.forward));
                    obj.GetComponent<Rigidbody>().AddForce(handler.BackSpawner.transform.forward * LengthForce + handler.BackSpawner.transform.up * HeightForce, ForceMode.Impulse);
                    obj.GetComponent<Rigidbody>().AddForce(-handler.BackSpawner.transform.up * GravityForce, ForceMode.Force);
                    break;
                }
            case PowerUpHandler.UseDirection.Forward:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(BobombObject, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
                    obj.GetComponent<Rigidbody>().AddForce(handler.FrontSpawner.transform.forward * LengthForce + handler.FrontSpawner.transform.up * HeightForce, ForceMode.Impulse);
                    obj.GetComponent<Rigidbody>().AddForce(-handler.FrontSpawner.transform.up * GravityForce, ForceMode.Force);
                    break;
                }
        }
    }
}
