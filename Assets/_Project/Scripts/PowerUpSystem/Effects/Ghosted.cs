﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Ghosted : Effect
{
    public float GhostDuration;

    public override void ApplyEffect(GameObject go)
    {
        go.transform.parent.GetComponentInChildren<PowerUpHandler>().ApplyGhost(GhostDuration);
    }
}
