﻿using MarioKart.Interfaces;
using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Disable : Effect
{
    [SerializeField]
    private float DisableDuration;

    public override void ApplyEffect(GameObject go)
    {
        base.ApplyEffect(go);
        go.GetComponent<IUncontrollable>().BlockControls(DisableDuration);
    }
}
