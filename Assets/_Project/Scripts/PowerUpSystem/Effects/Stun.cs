﻿using MarioKart.Interfaces;
using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Stun : Effect
{
    [SerializeField]
    private float StunDuration;

    public override void ApplyEffect(GameObject go)
    {
        base.ApplyEffect(go);
        go.GetComponent<IStunnable>().Stun(StunDuration);
    }
}
