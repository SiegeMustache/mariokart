﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropQuota
{
    public float AllocatedDistance;
    public PowerUpDrop[] Drops;

}

[System.Serializable]
public class PowerUpDrop
{
    public int Chance;
    public PowerUp PowerUp;
}
