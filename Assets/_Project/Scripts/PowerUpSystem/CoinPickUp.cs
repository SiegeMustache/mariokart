﻿using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{
    public GameObject CoinGFX;
    private Collider m_CoinCollider;
    public bool ShouldRespawn;
    [SerializeField]
    private float m_RespawnTime;
    private float m_RespawnTimer;

    private void Start()
    {
        m_CoinCollider = GetComponentInChildren<Collider>();
        SetCoin(true);
        m_RespawnTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(!ShouldRespawn)
        {
            if (m_RespawnTimer >= 0)
            {
                SetCoin(false);
                m_RespawnTimer -= Time.deltaTime;
            }
            else
            {
                SetCoin(true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!ShouldRespawn)
        {
            if (CoinGFX.activeSelf)
            {
                if (other.GetComponent<PowerUpHandler>())
                {
                    other.GetComponent<PowerUpHandler>().m_Mover.GetComponent<KartMover>().AddCoin();
                    Destroy(this.transform.gameObject);
                }
            }
        }
        else
        {
            if (CoinGFX.activeSelf)
            {
                if (other.GetComponent<PowerUpHandler>())
                {
                    other.GetComponent<PowerUpHandler>().m_Mover.GetComponent<KartMover>().AddCoin();
                    SetCoin(false);
                    m_RespawnTimer = m_RespawnTime;
                }
            }
        }
    }

    void SetCoin(bool value)
    {
        CoinGFX.SetActive(value);
        m_CoinCollider.enabled = value;
    }
}
