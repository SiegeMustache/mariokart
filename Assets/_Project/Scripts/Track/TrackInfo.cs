﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MarioKart.Track
{
[Serializable]
public class TrackInfo
{
    [SerializeField] private Vector3[] m_Points;

    [SerializeField, HideInInspector] private bool m_Closed = false;

    public Vector3[] Points => m_Points;

    public bool Closed => m_Closed;
    public int PointsCount => m_Points.Length;
    public int SegmentsCount => (m_Points.Length - 4) / 3 + 1;

    public TrackInfo(Vector3 center)
    {
        m_Points = new[]
        {
            center + Vector3.forward,
            center + ((Vector3.forward + Vector3.right) * .5f) * 5f,
            center + ((Vector3.forward + Vector3.left) * .5f) * 5f,
            center + Vector3.forward * 5f
        };
    }

    public void AddSegment(Vector3 anchorPos)
    {
        if (!Closed)
        {
            var lastSegment = GetPointsOfSegment(SegmentsCount - 1);

            Array.Resize(ref m_Points, m_Points.Length + 3);
            m_Points[m_Points.Length - 1] = anchorPos;
            m_Points[m_Points.Length - 2] = Vector3.Lerp(m_Points[m_Points.Length - 4], m_Points[m_Points.Length - 1], .25f);

            var controlPointDirection = lastSegment[2] - lastSegment[3];

            m_Points[m_Points.Length - 3] = lastSegment[3] - controlPointDirection;
        }
    }

    public Vector3[] GetPointsOfSegment(int i)
    {
        return new Vector3[] {m_Points[i * 3], m_Points[i * 3 + 1], m_Points[i * 3 + 2], m_Points[i * 3 + 3]};
    }

    public void MovePoint(int i, Vector3 position)
    {
        var delta = position - m_Points[i];
        if (Closed)
        {
            if (i == 0)
            {
                m_Points[PointsCount - 1] = position;
                m_Points[PointsCount - 2] += delta;
            }
            else if (i == PointsCount - 1)
            {
                m_Points[0] = position;
                m_Points[1] += delta;
            }
            else if (i == 1)
            {
                var movement = position - m_Points[0];
                m_Points[PointsCount - 2] = m_Points[0] - movement;
            }
            else if (i == PointsCount - 2)
            {
                var movement = position - m_Points[PointsCount - 1];
                m_Points[1] = m_Points[PointsCount - 1] - movement;
            }
        }

        if (i % 3f == 0f)
        {
            List<int> pointsToMove = new List<int>();
            if (i > 0)
            {
                pointsToMove.Add(i - 1);
            }

            if (i < PointsCount - 2)
            {
                pointsToMove.Add(i + 1);
            }

            foreach (var point in pointsToMove)
            {
                m_Points[point] += delta;
            }
        }
        else if (i % 3f == 1f % 3f)
        {
            if (i > 1)
            {
                var movement = position - m_Points[i - 1];
                m_Points[i - 2] = m_Points[i - 1] - movement;
            }
        }
        else if (i % 3f == 2f % 3f)
        {
            if (i < PointsCount - 2)
            {
                var movement = position - m_Points[i + 1];
                m_Points[i + 2] = m_Points[i + 1] - movement;
            }
        }

        m_Points[i] = position;
    }

    public void Open()
    {
        if (Closed)
        {
            Array.Resize(ref m_Points, m_Points.Length - 3);
            m_Closed = false;
        }
    }

    public void Close()
    {
        if (!Closed)
        {
            AddSegment(Points[0]);
            MovePoint(1, m_Points[1]);
            m_Closed = true;
        }
    }

    // public Vector3[] EvaluatePoints(float spacing)
    // {
    //     if (spacing <= 0f) throw new InvalidConstraintException("Negative Spacing is not allow");
    //
    //     var spacedPoints = new List<Vector3>();
    //     for (int segmentIndex = 0; segmentIndex < SegmentsCount; segmentIndex++)
    //     {
    //         var segmentPoints = GetPointsOfSegment(segmentIndex);
    //         var controlsLength = Vector3.Distance(segmentPoints[0], segmentPoints[1]) + Vector3.Distance(segmentPoints[1], segmentPoints[2]) + Vector3.Distance(segmentPoints[2], segmentPoints[3]);
    //         var segmentLength = Vector3.Distance(segmentPoints[0], segmentPoints[3]) + controlsLength / 2f;
    //
    //         int inSegmentCount = Mathf.CeilToInt(segmentLength / spacing);
    //         if (inSegmentCount <= 0)
    //             inSegmentCount = 1;
    //
    //         var minDistance = 1f / inSegmentCount;
    //         for (int i = 0; i < inSegmentCount; i++)
    //         {
    //             spacedPoints.Add(SplineFunctions.Cubic(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3], minDistance * i));
    //         }
    //     }
    //
    //     return spacedPoints.ToArray();
    // }
    
    public Vector3[] EvaluatePoints(float spacing, float resolution = 1)
    {
        List<Vector3> evenlySpacedPoints = new List<Vector3>();
        evenlySpacedPoints.Add(m_Points[0]);
        Vector3 previousPoint = m_Points[0];
        float dstSinceLastEvenPoint = 0;

        for (int segmentIndex = 0; segmentIndex < SegmentsCount; segmentIndex++)
        {
            Vector3[] p = GetPointsOfSegment(segmentIndex);
            float controlNetLength = Vector3.Distance(p[0], p[1]) + Vector3.Distance(p[1], p[2]) + Vector3.Distance(p[2], p[3]);
            float estimatedCurveLength = Vector3.Distance(p[0], p[3]) + controlNetLength / 2f;
            int divisions = Mathf.CeilToInt(estimatedCurveLength * resolution * 10);
            float t = 0;
            while (t <= 1)
            {
                t += 1f/divisions;
                Vector3 pointOnCurve = SplineFunctions.Cubic(p[0], p[1], p[2], p[3], t);
                dstSinceLastEvenPoint += Vector3.Distance(previousPoint, pointOnCurve);

                while (dstSinceLastEvenPoint >= spacing)
                {
                    float overshootDst = dstSinceLastEvenPoint - spacing;
                    Vector3 newEvenlySpacedPoint = pointOnCurve + (previousPoint - pointOnCurve).normalized * overshootDst;
                    evenlySpacedPoints.Add(newEvenlySpacedPoint);
                    dstSinceLastEvenPoint = overshootDst;
                    previousPoint = newEvenlySpacedPoint;
                }

                previousPoint = pointOnCurve;
            }
        }

        return evenlySpacedPoints.ToArray();
    }
}
}