﻿using UnityEngine;

namespace MarioKart.Track
{
public static class SplineFunctions
{
    public static Vector3 Quadratic(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        Vector3 p0 = Vector3.Lerp(a, b, t);
        Vector3 p1 = Vector3.Lerp(b, c, t);
        return Vector3.Lerp(p0, p1, t);
    }

    public static Vector3 Cubic(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
    {
        Vector3 p0 = Quadratic(a, b, c, t);
        Vector3 p1 = Quadratic(b, c, d, t);
        return Vector3.Lerp(p0, p1, t);
    }
}
}