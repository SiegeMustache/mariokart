﻿namespace MarioKart.Interfaces
{
public interface IMovable
{
    void Steer(float amount);
    void Accelerate(bool value);
    void Brake(bool value);
    void Drift(bool value);
}
}