﻿namespace MarioKart.Interfaces
{
public interface IStunnable
{
    void Stun(float time);
}
}