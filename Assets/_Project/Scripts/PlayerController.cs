﻿using MarioKart.Movement;
using MarioKart.Track;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Transform m_KartModelParent;

    private KartStats m_TargetKartStats;
    private LapCounter m_Lap;

    public LapCounter Lap
    {
        get
        {
            if (m_Lap == null)
                m_Lap = GetComponentInChildren<LapCounter>();
            return m_Lap;
        }
    }

    public KartStats TargetKartStats {
        get { return m_TargetKartStats; }
        set
        {
            m_TargetKartStats = value;
            if (m_KartModelParent.transform.childCount > 0)
                DestroyImmediate(m_KartModelParent.transform.GetChild(0).gameObject);

            Instantiate(m_TargetKartStats.KartPrefab, m_KartModelParent);
        }
    }
}
