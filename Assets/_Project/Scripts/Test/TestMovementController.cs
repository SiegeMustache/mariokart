﻿using System;
using MarioKart.Interfaces;
using UnityEngine;

namespace MarioKart.Test
{
[RequireComponent(typeof(Rigidbody))]
public class TestMovementController : MonoBehaviour, IMovable
{
    [SerializeField] private float m_SteeringSpeed = 45f;
    [SerializeField] private float m_MovementSpeed = 5f;
    
    private bool m_IsMoving = false;
    private float m_Steering = 0f;
    private Rigidbody m_Rb;

    public void Steer(float amount)
    {
        m_Steering = amount;
    }

    public void Accelerate(bool value)
    {
        m_IsMoving = true;
    }

    public void Brake(bool value)
    {
        m_IsMoving = false;
    }

    public void Drift(bool value)
    {
        throw new NotImplementedException();
    }

    private void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (m_IsMoving)
        {
            m_Rb.rotation *= Quaternion.Euler(0f, m_Steering * m_SteeringSpeed, 0f);
            m_Rb.AddForce(transform.forward * m_MovementSpeed, ForceMode.Force);
        }
    }
}
}