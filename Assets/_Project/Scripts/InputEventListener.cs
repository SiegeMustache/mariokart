﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InputEventListener : MonoBehaviour, ISelectHandler
{
    public UnityEvent Selected;


    public void OnSelect(BaseEventData eventData)
    {
        Selected?.Invoke();
    }
}
