﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using MarioKart.Interfaces;
using MLFramework;
using UnityEngine;

namespace MarioKart.Movement
{
[Serializable]
public struct KartDriftState
{
    public int KartPowerMin;
    public float BoostAmount;
    public List<GameObject> KartState;
}

public class KartMover : MonoBehaviour, IMovable, IBoostable, IStunnable, IUncontrollable
{
    private const float GRAVITY = 25;

    [SerializeField] private Transform m_KartModel;
    [SerializeField] private Transform m_ParticlesTransform;
    [SerializeField] private Rigidbody m_Sphere;

    [SerializeField] private float m_Acceleration = 65;
    [SerializeField] private float m_SteeringAngle = 15;

    [SerializeField] private List<KartDriftState> m_DriftStates;
    
    private float m_Speed;

    private float m_CurrentSpeed;

    private float m_Rotate;
    private float m_CurrentRotate;
    private float m_SteerValue = 0f;

    private bool m_Drifting;
    private int m_DriftDirection;
    private float m_DriftPower;

    private bool m_Boosting = false;
    private bool m_Stunned = false;

    private int m_CollectedCoins = 0;
    public bool Enabled = false;

    public int CollectedCoins => m_CollectedCoins;

    public void AddCoin()
    {
        m_CollectedCoins = Mathf.Clamp(m_CollectedCoins + 1, 0, 10);
    }

    public void RemoveCoin()
    {
        m_CollectedCoins = Mathf.Clamp(m_CollectedCoins - 1, 0, 10);
    }

    private void Start()
    {
        var childs = m_ParticlesTransform.GetComponentsInChildren<ParticleSystem>();

        foreach (var ps in childs)
        {
            ps.Stop();
        }
    }

    private void Update()
    {
        transform.position = m_Sphere.transform.position - new Vector3(0, 0.4f, 0);

        if (!m_Boosting)
        {
            m_CurrentSpeed = Mathf.SmoothStep(m_CurrentSpeed, m_Speed, Time.deltaTime * 12f);
            m_Speed = 0f;
        }

        m_CurrentRotate = Mathf.Lerp(m_CurrentRotate, m_Rotate, Time.deltaTime * 4f);
        m_Rotate = 0f;

        if (m_Drifting)
        {
            float control = (m_DriftDirection == 1)
                ? Utils.Remap(m_SteerValue, -1, 0, 1, 2)
                : Utils.Remap(m_SteerValue, -1, 2, 1, 0);
            float powerControl = (m_DriftDirection == 1)
                ? Utils.Remap(m_SteerValue, -1, .2f, 1, 1)
                : Utils.Remap(m_SteerValue, -1, 1, 1, .2f);
            m_Rotate = m_SteeringAngle * m_DriftDirection * control;

            var previousDriftPower = m_DriftPower;
            
            m_DriftPower += powerControl;

            var previousDriftState = 0;
            for (var i = 0; i < m_DriftStates.Count; i++)
            {
                if (previousDriftPower > m_DriftStates[i].KartPowerMin)
                {
                    previousDriftState = i;
                }
            }
            
            var driftState = 0;
            for (var i = 0; i < m_DriftStates.Count; i++)
            {
                if (m_DriftPower > m_DriftStates[i].KartPowerMin)
                {
                    driftState = i;
                }
            }

            if (driftState > previousDriftState)
            {
                foreach (var state in m_DriftStates[previousDriftState].KartState)
                {
                    //state.SetActive(false);
                    state.GetComponentInChildren<ParticleSystem>().Stop();
                }
                foreach (var state in m_DriftStates[driftState].KartState)
                {
                    //state.SetActive(true);
                    state.GetComponentInChildren<ParticleSystem>().Play();
                }
            }
        }

        //Animations

        if (m_Drifting)
        {
            float control = (m_DriftDirection == 1)
                ? Utils.Remap(m_SteerValue, -1, .5f, 1, 2)
                : Utils.Remap(m_SteerValue, -1, 2, 1, .5f);
            m_KartModel.parent.localRotation = Quaternion.Euler(0,
                Mathf.LerpAngle(m_KartModel.parent.localEulerAngles.y,
                    (control * m_SteeringAngle) * m_DriftDirection, .2f), 0);
        }
        else
        {
            m_KartModel.localEulerAngles = Vector3.Lerp(m_KartModel.localEulerAngles,
                new Vector3(0, 90 + (m_SteerValue * m_SteeringAngle), m_KartModel.localEulerAngles.z),
                .2f);
        }
    }

    private void FixedUpdate()
    {
        //Forward Acceleration
        if (m_Drifting)
            m_Sphere.AddForce(transform.forward * m_CurrentSpeed, ForceMode.Acceleration);
        else
            m_Sphere.AddForce(-m_KartModel.transform.right * m_CurrentSpeed, ForceMode.Acceleration);

        //Gravity
        m_Sphere.AddForce(-Vector3.up * GRAVITY, ForceMode.Acceleration);

        //Steering
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,
            new Vector3(0, transform.eulerAngles.y + m_CurrentRotate, 0), Time.deltaTime * 5f);
    }

    public void Steer(float amount)
    {
        if (Enabled)
        {
            m_SteerValue = amount;
            if (amount != 0)
            {
                m_Rotate = m_SteeringAngle * amount;
            }
        }
    }

    public void Accelerate(bool value)
    {
        if (Enabled)
        {
            if (value && !m_Stunned)
                m_Speed = m_Acceleration + m_Acceleration * m_CollectedCoins / 100;
        }
    }

    public void Brake(bool value)
    {
        if (Enabled)
        {
            if (value && !m_Stunned)
                m_Speed = -m_Acceleration - m_Acceleration * m_CollectedCoins / 100;
        }
    }

    public void Drift(bool value)
    {
        if (Enabled)
        {
            if (value)
            {
                if (!m_Drifting && m_SteerValue != 0)
                {
                    m_Drifting = true;
                    m_DriftDirection = m_SteerValue > 0 ? 1 : -1;

                    m_KartModel.DOComplete();
                    m_KartModel.DOPunchPosition(transform.up * .2f, .3f, 5, 1);
                    //m_ParticlesTransform.gameObject.SetActive(true);
                    foreach (var state in m_DriftStates[0].KartState)
                    {
                        state.GetComponentInChildren<ParticleSystem>().Play();
                    }

                }
            }
            else
            {
                if (m_Drifting)
                {
                    ApplyDriftBoost();
                }
            }
        }
    }

    public void SetDrift(bool value)
    {
            m_Drifting = value;
    }

    private void ApplyDriftBoost()
    {
        m_Drifting = false;

        if (m_DriftPower > m_DriftStates[0].KartPowerMin)
        {
            var driftState = 0;
            for (var i = 0; i < m_DriftStates.Count; i++)
            {
                if (m_DriftPower > m_DriftStates[i].KartPowerMin)
                {
                    driftState = i;
                }
            }
            
            if(m_DriftStates[driftState].BoostAmount > 0)
                AddBoost(2.5f, m_DriftStates[driftState].BoostAmount);
        }

        m_DriftPower = 0;

        //m_ParticlesTransform.gameObject.SetActive(false);
        foreach (var driftState in m_DriftStates)
        {
            foreach (var state in driftState.KartState)
            {
                //state.SetActive(false);
                state.GetComponentInChildren<ParticleSystem>().Stop();
            }
        }
        m_KartModel.parent.DOLocalRotate(Vector3.zero, .5f).SetEase(Ease.OutBack);
    }


    public void AddBoost(float amount, float time)
    {
        if (Enabled)
        {
            if (!m_Boosting)
            {
                m_CurrentSpeed = m_CurrentSpeed * amount;
                m_Boosting = true;
                DOVirtual.Float(m_CurrentSpeed * amount, m_CurrentSpeed, time, (x) => m_Speed = x + x * (m_CollectedCoins / 100)).onComplete += () => m_Boosting = false;
            }
        }
    }

    public void Stun(float time)
    {
        if (Enabled)
        {
            m_CurrentSpeed = 0;
            m_Stunned = true;
            var sqn = DOTween.Sequence();
            sqn.Append(m_KartModel.DOLocalRotate(new Vector3(0, 90, -180), time / 4f));
            sqn.Append(m_KartModel.DOLocalRotate(new Vector3(0, 90, 0), time / 4f));
            sqn.Insert(0f, m_KartModel.DOPunchPosition(transform.up * 2f, time / 2f, 5, 1));
            sqn.onComplete += () => m_Stunned = false;
            sqn.Play();
        }
    }

    public void BlockControls(float time)
    {
        m_Stunned = true;
        var sqn = DOTween.Sequence();
        m_KartModel.DOPunchRotation(new Vector3(0f, 135f, 0f), time).onComplete += () => m_Stunned = false;
    }
}
}