﻿using System;
using System.Collections;
using System.Collections.Generic;
using MarioKart.AI;
using MarioKart.Input;
using MarioKart.Track;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class EndRaceController : MonoBehaviour
{
    [SerializeField]
    private PlayerController m_Player;
    [SerializeField] private AIBrain m_EndGameAI;
    [SerializeField] private InputHandler m_InputHandler;
    [SerializeField] private Transform m_CameraTransform;
    [SerializeField] private Transform m_EndGameCameraTransform;

    private bool m_Triggered = false;

    private void Start()
    {
        m_Player.Lap.RaceEnd += OnEndRace;
    }

    private void OnEndRace()
    {
        m_Triggered = true;
        m_EndGameAI.enabled = true;
        m_InputHandler.enabled = true;
    }

    private void Update()
    {
        if(m_Triggered)
            m_CameraTransform.transform.position = Vector3.MoveTowards(m_CameraTransform.position, m_EndGameCameraTransform.position, 10f * Time.deltaTime);
    }
}
